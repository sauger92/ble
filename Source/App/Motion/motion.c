/**
 ******************************************************************************
 * @file    Source/App/Motion/motion.c
 * @author  Arthur Burnichon
 * @date    20-Oct-2017
 * @brief   Motion component implementation file
 ******************************************************************************
 * @attention
 *
 * &copy; COPYRIGHT(c) 2017 Arthur Burnichon - arthur@meetluseed.com
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "motion.h"
#include <math.h>
#include "stm32_debug.h"
#include <stdlib.h>
#include <string.h>

#include "hal_conf.h"
/* Private define ------------------------------------------------------------*/
/* Private types -------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/* Public functions ----------------------------------------------------------*/
/**
  * @brief  Motion initialization function
  * @param  None
  * @retval None
  */
float axesX;
float axesY;
float axesZ;
float BufferaxesX;
float BufferaxesY;
float BufferaxesZ;

uint8_t inclinationValue[4];
uint8_t moveValue;
uint8_t changeFlag;

void Motion_Init(void) {
	axesX=0;
		axesY=0;
		axesZ=0;
		BufferaxesX=0;
		BufferaxesY=0;
		BufferaxesZ=0;
		inclinationValue[0] = 0;
		inclinationValue[1] = 0;
		inclinationValue[2] = 0;
		inclinationValue[3] = 0;
		moveValue =0;
		changeFlag = 0;



}

/**
  * @brief  Processing function to detect motion event
  * @param  None
  * @retval None
  */
void Motion_Process(void) {
	if (BufferaxesX!=axesX || BufferaxesZ!=axesZ ||BufferaxesY!=axesY )
	{
		Motion_Value_Changed_Callback();
		BufferaxesX = axesX;
		BufferaxesY = axesY;
		BufferaxesZ = axesZ;

			  float total = sqrt(axesX*axesX+axesY*axesY+axesZ*axesZ);
			  float angleX = round(asin(axesX/ total )*180.0/3.1416);
			  float angleY = round(asin(axesY/ total )*180.0/3.1416);
			  float angleZ = round(acos(axesZ/ total )*180.0/3.1416);
			  //DEBUG_PRINTF("\r\nAngle_X: %d, Angle_Y: %d, Angle_Z: %d\r\n", (int)angleX, (int)angleY, (int)angleZ);
		if ((int)angleX<10 && (int)angleX>-10)
			{
			Motion_InclinationLeftRightAxisNone_CallBack();
			inclinationValue[0] = 0;
			inclinationValue[1] = 0;

			}
		if ((int)angleX>20 && (int)angleX<40)
			{
			Motion_InclinationLeftRightAxisRightMedium_CallBack();
			inclinationValue[0] = 2;
			inclinationValue[1] = 1;
			}
		if ((int)angleX>-40 && (int)angleX<-20)
			{
			Motion_InclinationLeftRightAxisLeftMedium_CallBack();
			inclinationValue[0] = 1;
			inclinationValue[1] = 1;
			}
		if ((int)angleX>50 && (int)angleX<70)
			{
			Motion_InclinationLeftRightAxisRightHigh_CallBack();
			inclinationValue[0] = 2;
			inclinationValue[1] = 2;
			}
		if ((int)angleX>-70 && (int)angleX<-50)
			{
			Motion_InclinationLeftRightAxisLeftHigh_CallBack();
			inclinationValue[0] = 1;
			inclinationValue[1] = 2;
			}

		if ((int)angleY<10 && (int)angleY>-10)
			{
			Motion_InclinationBackFrontAxisNone_CallBack();
			inclinationValue[2] = 0;
			inclinationValue[3] = 0;
			}
		if ((int)angleY>20 && (int)angleY<40)
			{
			Motion_InclinationBackFrontAxisFrontMedium_CallBack();
			inclinationValue[2] = 2;
			inclinationValue[3] = 1;
			}
		if ((int)angleY>-40 && angleY<-20)
			{
			Motion_InclinationBackFrontAxisBackMedium_CallBack();
			inclinationValue[2] = 1;
			inclinationValue[3] = 1;
			}
		if ((int)angleY>50 && (int)angleY<70)
			{
			Motion_InclinationBackFrontAxisFrontHigh_CallBack();
			inclinationValue[2] = 2;
			inclinationValue[3] = 2;
			}
		if ((int)angleY>-70 && (int)angleY<-50)
			{
			Motion_InclinationBackFrontAxisBackMedium_CallBack();
			inclinationValue[2] = 1;
			inclinationValue[3] = 2;
			}
		if (axesZ>1000)
		{
			Motion_MoveDown_CallBack();
		moveValue = 0;
		}
		if (axesZ<970)
		{
			Motion_MoveUp_CallBack();
			moveValue = 1;
		}
		changeFlag = 0;

	}

}

__weak void Motion_Value_Changed_Callback(void)
{

}
uint8_t* Motion_Inclination_GetValue(void){
	return inclinationValue;
}
uint8_t Motion_Move_GetValue(void){
	return moveValue;
}
__weak void Motion_InclinationLeftRightAxisNone_CallBack(void) {
  //FIXME Can be re-implemented to meet users needs
  //DEBUG_PRINTF("Inclination axis left right : none");
}

__weak void Motion_InclinationLeftRightAxisLeftMedium_CallBack(void) {
  //FIXME Can be re-implemented to meet users needs
  //DEBUG_PRINTF("Inclination axis left right : left medium");
}

__weak void Motion_InclinationLeftRightAxisLeftHigh_CallBack(void) {
  //FIXME Can be re-implemented to meet users needs
  //DEBUG_PRINTF("Inclination axis left right : left high");
}

__weak void Motion_InclinationLeftRightAxisRightMedium_CallBack(void) {
  //FIXME Can be re-implemented to meet users needs
  //DEBUG_PRINTF("Inclination axis left right : right medium");
}

__weak void Motion_InclinationLeftRightAxisRightHigh_CallBack(void) {
  //FIXME Can be re-implemented to meet users needs
  //DEBUG_PRINTF("Inclination axis left right : right high");
}

__weak void Motion_InclinationBackFrontAxisNone_CallBack(void) {
  //FIXME Can be re-implemented to meet users needs
  //DEBUG_PRINTF("Inclination axis back front : none");
}

__weak void Motion_InclinationBackFrontAxisBackMedium_CallBack(void) {
  //FIXME Can be re-implemented to meet users needs
  //DEBUG_PRINTF("Inclination axis back front : back medium");
}

__weak void Motion_InclinationBackFrontAxisBackHigh_CallBack(void) {
  //FIXME Can be re-implemented to meet users needs
  //DEBUG_PRINTF("Inclination axis back front : back high");
}

__weak void Motion_InclinationBackFrontAxisFrontMedium_CallBack(void) {
  //FIXME Can be re-implemented to meet users needs
  //DEBUG_PRINTF("Inclination axis back front : front medium");
}

__weak void Motion_InclinationBackFrontAxisFrontHigh_CallBack(void) {
  //FIXME Can be re-implemented to meet users needs
  //DEBUG_PRINTF("Inclination axis back front : front High");
}

__weak void Motion_MoveUp_CallBack(void) {
  //FIXME Can be re-implemented to meet users needs
  //DEBUG_PRINTF("Motion : Move Up");
}

__weak void Motion_MoveDown_CallBack(void) {
  //FIXME Can be re-implemented to meet users needs
  //DEBUG_PRINTF("Motion : Move Down");
}
